package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private static  final Properties PROPERTIES=new Properties();

    static {
        try {
            PROPERTIES.load(new FileReader("app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static final String URL=(String)PROPERTIES.get("connection.url");

    public static synchronized DBManager getInstance() {
        if(instance!=null)return instance;

        instance=new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> list = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM users")) {

            while (rs.next()) {
                User tempUser = User.createUser(rs.getString("login"));

                tempUser.setId(rs.getInt("id"));
                list.add(tempUser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return list;
    }

    public boolean insertUser(User user) throws DBException {
        boolean res = false;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement()
        ) {
            stmt.executeUpdate("delete from users where login='"+user.getLogin()+"'");
            int n = stmt.executeUpdate("INSERT INTO users VALUES (DEFAULT, '"+user.getLogin()+"')",Statement.RETURN_GENERATED_KEYS);

            ResultSet rs=stmt.getGeneratedKeys();
            if(rs.next())user.setId(rs.getInt(1));
            if (n > 0) res = true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return res;
    }

    public boolean deleteUsers(User... users) throws DBException {
        boolean res = false;
        int n = 0;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement()
        ) {
            for (User user : users
            ) {int id=getUser(user.getLogin()).getId();
                n += stmt.executeUpdate("delete  from users where login='" + user.getLogin() + "'");
                stmt.executeUpdate("delete  from users_teams where user_id="+ id +"");
            }

            if (n > 0) res = true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return res;

    }

    public User getUser(String login) throws DBException {
        User user = null;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM users where login='" + login + "'")) {

            if (rs.next()) {
                user = User.createUser(rs.getString("login"));

                user.setId(rs.getInt("id"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM teams where name='" + name + "'")) {

            if (rs.next()) {
                team = Team.createTeam(rs.getString("name"));

                team.setId(rs.getInt("id"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM teams")) {

            while (rs.next()) {

                Team tempTeam = Team.createTeam(rs.getString("name"));

                tempTeam.setId(rs.getInt("id"));
                teams.add(tempTeam);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean res = false;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement()
        ) {
             stmt.executeUpdate("delete from teams where name='"+team.getName()+"'");

            int n =  stmt.executeUpdate( "INSERT INTO teams VALUES (DEFAULT, '"+team.getName()+"')",Statement.RETURN_GENERATED_KEYS);
            ResultSet rs=stmt.getGeneratedKeys();
            if(rs.next())team.setId(rs.getInt(1));
            if(n>1)res=true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return res;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        boolean result=false;
        Connection con=null;
        Statement stmt=null;
        try { con = DriverManager.getConnection(URL);
            con.setAutoCommit(false);
            if(!findAllUsers().contains(user)||!findAllTeams().containsAll(Arrays.asList(teams)))throw new SQLException();
            int n=0;
            for (Team tempTeam:teams) {

                stmt = con.createStatement();
                n+=stmt.executeUpdate("INSERT INTO users_teams VALUES ("+user.getId()+", "+tempTeam.getId()+")");

            }
            if(n>0)result=true;
            con.commit();

        }catch (SQLException e) {
            rollback(con);
            e.printStackTrace();
            throw new DBException("transaction failed",e);

        }finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }



        return result;
    }

    private void rollback(Connection con) {
        if(con !=null){
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = findAllTeams();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM users_teams where user_id="+user.getId()+"")) {
                List<Team>tempList=new ArrayList<>();

                while (rs.next()) {

                for (Team team:list){
                if(team.getId()== rs.getInt("team_id")){
                    tempList.add(team);
                break;
                }
            }

            }
                list=tempList;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean res = false;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement()
        ) {
            int n=stmt.executeUpdate("delete from teams where name='"+team.getName()+"'");

            stmt.executeUpdate( "delete from users_teams where team_id="+team.getId()+"");
            if(n>0)res=true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return res;
    }

    public boolean updateTeam(Team team) throws DBException {
        boolean res = false;
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement()
        ) {
            int n=stmt.executeUpdate("update teams set name='"+team.getName()+"' where id="+team.getId()+"");


            if(n>0)res=true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("oups", e);
        }
        return res;
    }

}
